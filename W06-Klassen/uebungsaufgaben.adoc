= Übungsaufgaben
:sectnums:
:stem: latexmath
:icons: font
:hide-uri-scheme:

== Modellierung als Objekte (Vorlesung)

Überlegen Sie sich zu zweit, welche Daten zu folgenden Objekten gehören und welches Verhalten man dazu programmieren könnte.

[loweralpha]
. Ein Film auf Netflix
. Eine Spielkarte in einem Online-Pokerspiel
. Ein Kommentar auf https://www.20min.ch
. Ein Modulanlass im Einschreibungssystem der HT
. Ein eigenes Beispiel für ein Objekt in einem Softwaresystem


== Instanz-Methode schreiben (Vorlesung)

In der Vorlage von dieser Woche finden Sie eine Version der `Modul`-Klasse und ein paar Unit-Tests. Ändern Sie die Implementation der Methode `endNote`, sodass sie folgende Anforderungen erfüllt:

. Die Endnote soll auf eine Nachkommastelle gerundet werden.
+
*Tipp:* `Math.round(x)` rundet eine Zahl `x` auf eine ganze Zahl.

. Falls die Erfahrungsnote oder die MSP-Note noch 0 ist, wird als Endnote auch 0 zurückgegeben werden.


== Klassen und Konstruktoren (Vorlesung)

Finden und erklären Sie die Fehler in folgenden Klassen:

[loweralpha]
. {empty}
+
[source,java]
----
public class Land {
    String name;
    int einwohner;

    public Land(String name, int einw) {
        this.name = name;
        einwohner = einw;
    }
}
----
. {empty}
+
[source,java]
----
public class Color {
    int red;
    int blue;
    int green;

    public Color(int r, int g, int b) {
        r = red;
        b = blue;
        g = green;
    }
}
----
. {empty}
+
[source,java]
----
public class Point2D {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
----
. {empty}
+
[source,java]
----
public class Line {
    Point2D start;
    Point2D end;
    Color width;

    public void Line() {}
}
----

