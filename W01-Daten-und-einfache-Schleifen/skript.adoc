= Daten & einfache Schleifen
:sectnums:
:appendix-caption: Anhang
:stem: latexmath

== Java-Basics

=== Grundprogramm

Java ist eine objekt-orientierte Sprache. Im Moment bedeutet das erst einmal, dass sämtliche Befehle in einem Java-Programm in _Klassen_ (`class`) stehen müssen. Jede Klasse wird in einer Datei gespeichert, die gleich heisst wie die Klasse. Das Programm im Beispiel unten muss also in einer Datei namens `HelloWorld.java` gespeichert sein.

Dieses klassische «Hello World»-Programm ist meist das erste, welches in einer neuen Programmiersprache geschrieben wird. Es zeigt auch die Minimalform eines Java-Programms. Die Befehle (oder _Anweisungen_) stehen in  sogenannten `main`-Methode:

[source,java]
----
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World");
    }
}
----

Vorerst haben alle unsere Programme diese Form. Abgesehen vom Namen des Programms (hier `HelloWorld`) und den Befehlen zwischen den inneren `{` `}` ist alles unveränderlich. Sie müssen diese Form genau einhalten, sonst funktioniert das Programm nicht. Später werden wir sehen, was die Dinge wie `public`, `static` usw. bedeuten.

Folgendes ist beim Programmieren in Java wichtig zu wissen:

* Die Gross- und Kleinschreibung sind von Bedeutung (Case-sensitivität).
* Einzelne Anweisungen werden mit einen Strichpunkt `;` abgeschlossen.
* Anweisungen können mit `{}` zusammengefasst werden. Zu jeder `{` gehört auch eine `}`.
* Die Reihenfolge der einzelnen Anweisungen ist relevant; die Anweisungen werden von oben nach unten ausgeführt.

=== String-Literale und `System.out.println()`

Im «Hello World»-Programm wird ein _String_ (oder genauer _String-Literal_) verwendet; das ist der Text zwischen den Anführungszeichen `""`. Ein solcher Text unterscheidet sich vom restlichen Programmtext dadurch, dass er eins zu eins vom Programm selber verwendet wird (und nicht z. B. als Befehl interpretiert wird).

Strings müssen immer von «normalen» doppelten Anführungszeichen umgehen sein; einfache Anführungszeichen `''` oder spezielle Anführungszeichen wie `«»` oder `“”` funktionieren nicht. Zudem kann ein String nicht über mehrere Zeilen gehen.

Im «Hello World»-Programm wird der String-Text direkt auf der Konsole ausgegeben. Dafür wird die Anweisung `System.out.println(...)` verwendet. Das «`ln`» steht für _line_, d. h. der Text wird auf einer eigenen Zeile ausgegeben. Zwischen den Klammern steht das, was ausgegeben werden soll. Im «Hello World»-Programm ist das ein einfacher String, aber es können auch Zahlen oder sogar die Resultate von Rechenoperation ausgegeben werden (dazu unten mehr):

*Beispiele:*
[%autowidth]
|===
|Befehl                         |Ausgabe

|`System.out.println("Hallo");` |`Hallo`
|`System.out.println(42)`       |`42`
|`System.out.println(1 + 2)`    |`3`
|`System.out.println()`         |(leere Zeile)
|===


=== Kommentare

Kommentare sind Zusatztext in einem Programm, um dessen Lesbarkeit zu erhöhen. Kommentare werden vom Compiler ignoriert, aber sind ein wichtiges Hilfsmittel bei der Programmierung: Sie dienen dazu, dass der Code gut dokumentiert ist, und somit von anderen Personen (oder einem zukünftigen Selbst) wiederverwendet werden kann. Es gibt zwei Möglichkeiten einen Kommentar in ein Programm einzufügen:

[%autowidth]
|===
|Zeichen  |Verwendung

|`+/* … */+`
|Der Kommentar kann irgendwo im Programm beginnen und enden (auch mitten in einer Anweisung) und über mehrere Zeilen gehen

|`+// …+`
|Der Kommentar geht bis zum Ende der Zeile
|===

*Ausnahme:* Innerhalb von Strings werden die Zeichen `+/*+` und `+*/+` nicht als Teil eines Kommentars, sondern als normalen Text interpretiert.


== Turtle Graphics

Um in den ersten Wochen interessantere Programme schreiben zu können, verwenden wir eine Zusatzbibliothek, welche es erlaubt, einfache Grafiken zu programmieren. Die Bibliothek erlaubt es, eine Roboter-Schildkröte, die _Turtle_, zu steuern. Wir stellen uns vor, dass die Turtle einen Stift dabei hat, mit dem sie auf den Boden zeichnet. Indem man der Turtle Anweisungen gibt wie «Gehe 100 Pixel vorwärts» oder «Drehe dich um 90° nach rechts», erzeugt man eine Zeichnung.

Das folgende Beispielprogramm erzeugt ein gleichseitiges Dreieck:

[source,java]
----
import static ch.trick17.turtle4j.TurtleGraphics.*;

public class Dreieck {
    public static void main(String[] args) {
        forward(100);
        left(120);
        forward(100);
        left(120);
        forward(100);
    }
}
----

Turtle-Programme unterscheiden sich von «normalen» Programmen in zwei wichtigen Aspekten. Erstens funktionieren sie nur in den Projektvorlagen, die wir zur Verfügung stellen; in Java-Projekten, die Sie selber erstellen, fehlt die Turtle-Zusatzbibliothek. Zweitens funktionieren die Turtle-Befehle nur, wenn oben im Programm die korrekte `import`-Anweisung steht (siehe Beispiel oben).

Die Turtle akzeptiert folgende Befehle:

[%autowidth]
|===
|Befehl  |Beschreibung |Beispiele

|`forward`
|Bewege dich um so viele Pixel vorwärts
|`forward(100)` +
`forward(2.5)`

|`back`
|Bewege dich um so viele Pixel rückwärts
|`back(50)` +
`back(200)`

|`left`
|Drehe dich um so viele ° nach links
|`left(90)` +
`left(22.5)`

|`right`
|Drehe dich um so viele ° nach rechts
|`right(45)` +
`right(120)`

|`penUp`
|Bewege dich ab jetzt ohne zu zeichnen
|`penUp()`

|`penDown`
|Zeichne ab jetzt wieder beim Bewegen
|`penDown()`

|`setPenColor`
|Verwende ab jetzt eine andere Stiftfarbe. Farben werden als Strings angegeben, entweder als https://de.wikipedia.org/wiki/Webfarbe#CSS_3[englischer Name], als Hex-String oder mittels RGB- oder HSL-Format.
|`setPenColor("blue")` +
`setPenColor("HotPink")` +
`setPenColor("#ff6688")` +
`setPenColor("rgb(255,102,136)")` +
`setPenColor("hsl(240,100%,100%)")` +

|`setPenWidth`
|Verwende ab jetzt eine andere Stiftbreite. Die Breite wird in Pixel angegeben; der Standardwert ist 1.
|`setPenWidth(3)` +
`setPenWidth(0.5)` +

|`setSpeed`
|Bewege dich ab jetzt schneller oder langsamer. Die Geschwindigkeit wird in Pixel pro Sekunde angegeben; der Standardwert ist 100.
|`setSpeed(50)` +
`setSpeed(1000)`
|===


== Datentypen und Ausdrücke

Die meisten Programme werden verwendet, um irgendwelche Arten von Daten zu verarbeiten, z. B. Text, Messwerte, Bild- oder Audiodaten.

Die einfachste Art von Datenverarbeitung sind einfache Rechenoperationen. In Java kann man überall, wo eine Zahl gebraucht wird, auch eine Rechnung hinschreiben. In einem Turtle-Programm zum Beispiel:

[source,java]
----
forward(2 * 100);
right(360 / 4);
----

(`*` steht für Multiplikation, `/` für Division.)

In Java überprüft der Compiler dabei immer, ob die ausgeführten Operationen für die verwendeten Daten Sinn machen; Java ist eine sogenannte _typsichere_ Sprache. Zahlen kann man z. B. multiplizieren, dividieren, usw., während das mit Strings oder Wahr-/Falsch-Werten (eine andere Art von Daten) nicht geht. Versucht man das trotzdem, spricht man von einem _Typfehler_. Folgende Anweisungen wären z. B. fehlerhaft:

[source,java]
----
forward("einen Meter"); // forward-Anweisung braucht Zahl, nicht String
----

[source,java]
----
System.out.println("2000" / 3); // Strings kann man nicht dividieren
----

Bei einer typsicheren Sprache wie Java ist es nun so, dass der Compiler solche Fehler bemerkt und fehlerhafte Programme gar nicht erst übersetzt. Dadurch kann man es auch nicht ausführen. In gewissen anderen Programmiersprachen kann man Programme mit Typfehler hingegen durchaus ausführen -- sie stürzen aber ab, sobald eine Anweisung mit einem Typfehler ausgeführt wird.


=== Primitive Datentypen

Java enthält einige vordefinierte Datentypen für einfache (nicht zusammengesetzte) Werte. Diese heissen _primitive Typen_ und unterscheiden sich in der Art der Interpretation der gespeicherten Daten und in der Grösse.

Die folgende Tabelle gibt einen Überblick über die primitiven Datentypen in Java. Viele davon kommen auch in anderen Programmiersprachen vor.

[%autowidth]
|===
|Typ |Beschreibung |Grösse in Bit |Wertebereich

|`boolean` |Wahr-/Falsch-Wert |8 |`true` oder `false`
|`char` |Zeichen |16 |Unicode-Zeichen (siehe nächste Woche)
|`byte` |Ganzzahl |8 |-128 … +127 + -2^7^ … +(2^7^-1)
|`short` |Ganzzahl |16 |-32‘768 … +32‘767 + -2^15^ … +(2^15^-1)
|`int` |Ganzzahl |32 |-2‘147‘483‘648 …+2‘147‘483‘647 bzw. -2^31^ … +(2^31^-1)
|`long` |Ganzzahl |64 a| -9‘223‘372‘036‘854‘775‘808…-9‘223‘372‘036‘854‘775‘807 bzw. -2^63^ … +(2^63^-1)
|`float` |Gleitkommazahl |32 |+/- 3.40282347*10^38^
|`double` |Gleitkommazahl |64 |+/- 1.79769313486231570*10^308^
|===

Die üblicherweise verwendeten Datentypen sind `int`, `double`, `boolean` und `char`. Die restlichen werden nur in Spezialfällen benötigt.

=== Ausdrücke

Eine Rechnung in einem Programm nennt man _Ausdruck_. Ausdrücke in Java können beliebig kompliziert sein, z. B.

[source,java]
----
(10 * 0.25) + (6 * 0.5)
----

Aber auch einfache Zahlen (_Literale_) wie `4` oder `0.25` sind Ausdrücke, genauso wie Strings. Jeder Ausdruck hat einen Typ. Einfachen Zahlen sieht man den Typ direkt an: `4` ist eine ganze Zahl, hat also Typ `int`, und `0.25` ist eine Kommazahl und hat damit Typ `double`. Bei zusammengesetzten Ausdrücken bestimmt der Compiler den Typ aufgrund der verwendeten Operationen (siehe unten).

*Beispiele:*

[%autowidth]
|===
|Ausdruck        | Typ

|`-100`          | `int`
|`2 + 3`         | `int`
|`3.1415`        | `double`
|`10.0`          | `double`
|`(2.5 + 2) * 4` | `double` (siehe unten)
|`"hello"`       | `String` (siehe nächste Woche)
|===

Ausdrücke sind keine vollständigen Anweisungen, sondern sind immer Teil einer Anweisung. Man kann einen Ausdruck beispielweise mittels `System.out.println` auswerten und das Resultat auf der Konsole ausgeben:

[source,java]
----
System.out.println((10 * 0.25) + (6 * 0.5));
----

Man nennt die Werte, die verrechnet werden, _Operanden_ und die Rechenoperationen _Operatoren_. In diesem Beispiel sind `10` und `0.25` die Operanden für den ersten `+*+`-Operator, `6` und `0.5` die Operanden für den zweiten `+*+`-Operator. Die Operanden für den `+`-Operator sind die geklammerten Teilausdrücke links und rechts davon.

Für die Datentypen `int` und `double` stehen (unter anderem) folgenden Operatoren zur Verfügung:

[%autowidth]
|===
|Operator |Bedeutung |Beispiel | Resultat

|`+` |Addition                    | `2 + 2`     | 4
|`-` |Subtraktion                 | `53 - 18`   | 35
|`*` |Multiplikation              | `3 * 8`     | 24
|`/` |Division                    | `4.8 / 2.0` | 2.4
|`%` |Divisions-Rest oder «Modulo»| `19 % 5`    | 4
|===

Bei den Rechenoperationen gibt es einen Unterschied zwischen Operationen im ganzzahligen und im Gleitkomma-Bereich. Bei einer ganzzahligen Division ist auch das Resultat eine ganze Zahl. Ist hingegen mindestens ein Operand ein Gleitkommawert (`double` oder `float`), ist auch das Resultat ein Gleitkommawert.

*Beispiel:*

[source,java]
----
System.out.println(5 / 4);      // gibt 1
System.out.println(5 / 4.0);    // gibt 1.25
System.out.println(5.0 / 4);    // gibt 1.25
System.out.println(5.0 / 4.0);  // gibt 1.25
----


=== Mehrdeutigkeit und Präzedenz

Bei Ausdrücken mit mehreren Operatoren, z. B. `2 + 3 * 4` stellt sich die Frage, in welcher Reihenfolge die Operationen durchgeführt werden. Würde zuerst das `+` durchgeführt, wäre das Resultat nämlich 20, andernfalls 14. In natürlichen Sprachen wie Deutsch kommt es manchmal zu solchen Mehrdeutigkeiten, z. B. «der Mann auf dem Berg mit dem Teleskop». Hier ist nicht ohne weiteres klar, ob der Mann das Teleskop bei sich hat oder ob es auf dem Berg steht.

Gute Programmiersprachen sind hingegen so aufgebaut, dass keinerlei Mehrdeutigkeiten entstehen können. In Java wird das Problem von Ausdrücken mit mehreren Operatoren durch _Präzedenz-Regeln_ gelöst:

* Jede Art von Operator hat eine bestimmte _Präzedenz_, die angibt, ob ein Operator früher oder später als andere ausgeführt wird. Man sagt auch, die Präzedenz gibt an, wie «start ein Operator bindet».
* Falls mehrere Operatoren mit der selben Präzedenz vorkommen, werden sie in einer bestimmten Richtung ausgeführt (die meisten von links nach rechts).

Im Ausdruck `2 + 3 * 4` hat der `*`-Operator eine höhere Präzedenz als das `+`, das heisst, es wird zuerst `3 * 4` (gibt 12) gerechnet und dann `2 + 12` (gibt 14). Das entspricht der Regel «Punkt vor Strich» aus der Mathematik (auch wenn die Operatoren in Java eigentlich alle aus Strichen bestehen 😉).

Die folgende Tabelle gibt die Präzedenz des bisherigen Operatoren an. Später wird sie durch weitere Operatoren erweitert.

[%autowidth]
|===
|Beschreibung                        |Operatoren |Präzedenz

|_unäre_ Operatoren (z. B. `-4`) |`+`, `-`      | höchste
|multiplikative Operatoren           |`*`, `/`, `%` |
|additive (binäre) Operatoren        |`+`, `-`      | niedrigste
|===


== Variablen

Variablen erlauben es, Werte zwischenzuspeichern, die man im Programm später wieder braucht. Eine Variable hat einen Namen, über den sie angesprochen wird, und einen fixen Datentyp. Der Wert, der in einer Variable gespeichert ist, kann sich im Verlaufe des Programms ändern (er kann _variieren_, daher der Name), aber es kann immer nur ein Wert des deklarierten Datentyps sein.

=== Deklaration

Bevor eine Variable in einem Programm verwendet werden kann, muss sie deklariert werden. Das heisst, dass Sie als Programmiererin oder Programmierer einen Speicherbereich für einen bestimmten Datentyp belegen und diesem Speicherplatz einen Namen geben. Über diesen Namen kann der Speicherbereich während des Programmablaufs aufgerufen werden. Namen von Variablen beginnen im Java gemäss Konvention jeweils mit einem Kleinbuchstaben, sie dürfen keine Leerzeichen enthalten und sollten möglichst aussagekräftig sein. Wenn ein Name aus mehreren Wörtern besteht, werden die Wörter ab dem zweiten Wort mit einem Grossbuchstaben am Anfang geschrieben. Für mehr Informationen können Sie in den _Java Coding Conventions_ nachschauen (z. B. im https://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html[Java-Tutorial])

*Deklaration:*

[source,java]
Datentyp name;

*Beispiel:*

[source,java]
----
int meineZahl;
double meineKommaZahl;
----

Mehrere Variablen vom gleichen Typ können auch wie in folgendem Beispiel in einer Deklaration geschrieben werden:

[source,java]
----
int meineZahl1, meineZahl2, meineZahl3;
----

=== Initialisierung und Wertzuweisung

Variablen sind nach der Deklaration erst einmal _uninitialisiert_, d. h. es ist noch kein Wert darin gespeichert. Das Speichern von Werten geschieht mit dem Zuweisungsoperator. In Java wird hierfür ein Gleichheitszeichen (=) verwendet. Dabei wird der Wert des Ausdrucks rechts des Zuweisungsoperators in der Variablen auf der linken Seite gespeichert. Wenn einer Variable das erste Mal ein Wert zugwiesen wird, spricht man von der _Initialisierung_.

*Schreibweise:*

Zuweisungsoperator (=)

[source,java]
----
variable = wert;
----

*Beispiel:*

[source,java]
----
x = 4;
----

*Achtung:* «Variablen» und die Schreibweise stem:[x = 4] kennt man auch aus der Mathematik. Dort bedeutet stem:[=] aber etwas leicht anderes, nämlich (ewige) Gleichheit. Im Programmieren handelt es sich bei einer Zuweisung `x = 4;` aber um einen _einmaligen_ Speichervorgang, mit dem Resultat, dass sich der Wert der Variable `x` ändert. Bis auf Weiteres sind `x` und `4` zwar nun auch gleich, aber das kann sich mit einer späteren Zuweisung, z. B. `x = 7;` wieder ändern.

Damit einer Variablen ein Wert zugewiesen werden kann, darf die Variable nicht als Konstante definiert sein (siehe <<_konstanten>>) und der Typ des Werts muss zum Typ der Variablen kompatibel sein. Auf jeden Fall kompatibel sind Variablen und Wert desselben Datentyps. Zusätzlich kompatibel für eine Zuweisung sind Variablentypen, deren Datentyp auf der linken Seite eine Verallgemeinerung des Typs auf der rechten Seite darstellt. Folgende Abbildung zeigt die Richtung, in der Datentypen in Java automatisch kompatibel sind:
 
`byte` -> `short` -> `int` -> `long` -> `float` -> `double`

`char` -> `int`
 

*Beispiel:*
Ein Wert vom Typ `int` kann einer Variable vom Typ `double` zugewiesen werden:

[source,java]
----
int ganzeZahl;
double kommaZahl;
ganzeZahl = 4;
kommaZahl = ganzeZahl;
----

Möchte man eine Zuweisung machen, welche entgegen der Pfeilrichtung ist, muss eine Typenkonvertierung durchgeführt werden, das sogenannte Typecasting. Die Programmiererin/der Programmierer ist dabei selber dafür verantwortlich, dass die Zuweisung möglich ist, bzw. nimmt in Kauf, dass gewisse Informationen verloren gehen oder das Resultat nicht brauchbar ist.

*Beispiel:*

[source,java]
----
int kleineZahl = 1;
byte nochKleinereZahl = (byte) kleineZahl;
----

Eine Variable kann in einem Programm verschiedene Gültigkeitsbereiche besitzen, das heisst z.B. nur in einem bestimmten Bereich des Programms gelten. Weiteres dazu erfahren Sie in einem späteren Modul.

=== Literale

Den ganzzahligen Datentypen können auch Werte zugewiesen werden, welche hexadezimal oder oktal angegeben werden.

*Hexadezimal:* Hexadezimalen Werten wird das Literal 0x bzw, 0X vorausgestellt. Danach folgt die Zahl in der hexadezimalen Darstellung. Gross-/Kleinschreibung spielt hier keine Rolle.

[source,java]
----
int i = 0xab71;  // kleines x
int a = 0X6A;    // grosses X
----

*Oktal:* Oktalen Werten wird eine Null vorangestellt.

[source,java]
----
int z = 0526;   // z hat nur den dezimalen Wert 342.
----

=== Konstanten

Konstanten werden wie Variablen mit einem Namen bezeichnet. Sie enthalten während der Programmausführung immer den gleichen Wert. Es kann also nach der Initialisierung keine weitere Wertzuweisung vorgenommen werden. Konstanten können jedoch Teil einer Wertzuweisung an Variablen sein. Eine Konstante wird zusätzlich zu Namen und Datentyp mit *final* deklariert. Konstanten werden gemäss Konvention mit Grossbuchstaben geschrieben. Wörter werden mit einem Unterstrich getrennt.

*Schreibweise:*

[source,java]
----
final Datentyp NAME;
----

*Beispiel:*

[source,java]
----
final int MEINE_KONSTANTE = 4;
----


=== Ausgabe auf der Konsole
Variablenwerte können auch auf der Konsole ausgegeben werden. Typischerweise auch noch in Kombination mit einem Text, der anzeigt, um welche Variable es sich handelt.

[source,java]
----
System.out.println("Das ist ein Text");
int value = 9;
System.out.println(value);
System.out.println("Value hat den Wert: " + value);
----

Das Resultat dieses Codes ist wie folgt auf der Konsole sichtbar:

----
Das ist ein Text
9
Value hat den Wert: 9
----

Bei der Ausgabe wird die Variable durch ihren Wert ersetzt, und dieser dann auf der Konsole angezeigt. Mit dem `+` können Strings mit anderen Strings oder eben auch mit Variablen zu einem ganzen Text verbunden werden. (Das ist eine Ausnahme; die restlichen arithmetischen Operatoren funktionieren nicht für Strings.)

== Einfache Schleifen mit `for`

Es kommt oft vor, dass bestimmte Anweisungen mehrmals ausgeführt werden sollen. Um zum Beispiel ein Achteck mit der Turtle zu zeichnen, müsste man 8× `forward` und `right` ausführen:

[source,java]
----
forward(50);
right(45);
forward(50);
right(45);
forward(50);
right(45);
...
----

Solche Wiederholungen kann man mit einer _Schleife_ ausdrücken, eines der wichtigsten Konstrukte beim Programmieren. Eine Art von Schleifen heisst `for`-Schleife und sieht so aus:

[source,java]
----
for (int i = 1; i <= 8; i = i + 1) {
    forward(50);
    right(45);
}
----

Dieses Stück Code macht folgende Dinge:

. _Initialisierung_: Eine Variable `i` wird erstellt und mit dem Wert `1` initialisiert.
. _Test_: Es wird geprüft, ob der Code zwischen den `{}` ausgeführt werden soll; das macht der Ausdruck `+i <= 8+`. Nur wenn `i` kleiner oder gleich `8` ist, wird der Code ausgeführt.
. _Body_: Der Code zwischen den `{}` wird ausgeführt (falls der Test erfolgreich war).
. _Update_: Die Variable `i` wird um `1` erhöht.
. Zurück zum Schritt 2.

Die Variable `i` wird also verwendet, um zu zählen, wie viele Wiederholungen schon gemacht wurden. Sobald `i` den Wert 9 enthält (nach 8 Wiederholungen) schlägt der Test fehl und die Schleife ist zu Ende. Eigentlich ist das Ganze nur eine komplizierte Art zu sagen: «Wiederhole dieses Stück Code 8×».

Der grosse Vorteil von Schleifen ist, dass der Code immer (etwa) gleich gross ist, egal wie viele Wiederholungen gemacht werden sollen. Eine Schleife mit _einer Million_ Wiederholungen ist genauso einfach zu schreiben:

[source,java]
----
for (int i = 1; i <= 1000000; i = i + 1) {
    // Code
}
----



[appendix]
== Darstellung von Zahlen und Zeichen im Computer

Um die Darstellung von Zeichen, Zahlen und Texten im Computer zu verstehen, muss man das binäre System verstehen.

=== Binäres System

Alle Rechner stellen Information im binären System dar. Dieses kennt nur zwei Ziffern nämlich 0 und 1 (im Gegensatz zum Dezimalsystem mit dem Ziffern 0 bis 9). Eine solche Ziffer wird als Bit bezeichnet (Abkürzung für [.underline]##Bi##nary Digi[.underline]##t## übersetzt „Binäre Ziffer“). Ein Bit stellt den kleinsten speicherbaren Wert in einem Computer dar. Jeweils 8 Bits werden zu einem Byte zusammengefasst. Ein Byte kann somit 2^8^ = 256 verschiedene Sequenzen von je 8 Bit speichern.

=== Darstellung von Zahlen im binären System

Betrachten wir folgende 8-Bit-Zahl mit dem Bitmuster 01011011. Da eine solche Zahl wenig lesefreundlich wäre, wird sie als Dezimalzahl (91) dargestellt:

[cols=",,,,,,,,,",options="header",]
|===
|*Bit* |*8* |*7* |*6* |*5* |*4* |*3* |*2* |*1* |
|Binärwert |*0* |*1* |*0* |*1* |*1* |*0* |*1* |*1* |
|Wertigkeit |128 |64 |32 |16 |8 |4 |2 |1 |
|Dezimalwert |0 |64 |0 |16 |8 |0 |2 |1 |91
|===

Im Gegensatz zur Mathematik, wo für jede Zahl eine nächst höhere existiert, gibt es bei Binärzahlen im Computer immer eine obere Grenze. Eine 8-Bit-Zahl, wie in unserem Beispiel, kann Werte zwischen 00000000 und 11111111 (256 im Dezimalsystem) speichern. Mit anderen Worten, wir können im binären System mit 8 Bit nur die ganzen Zahlen 0 bis 255 darstellen. Ist die Zahl, die wir darstellen wollen, grösser, müssen wir einen grösseren Speicherbereich bereitstellen (siehe Kapitel Datentypen weiter unten).

[appendix]
== Operatoren

.Operatoren mit Rang, Typ und Beschreibung von https://openbook.rheinwerk-verlag.de/javainsel/02_004.html#u2.4
[cols="m,10,10,~",options="header",]
|===
|Operator |Rang |Typ |Beschreibung
|++, \--     |1      |arithmetisch   |Inkrement und Dekrement
|+, -       |1      |arithmetisch   |unäres Plus und Minus
|~          |1      |integral       |bitweises Komplement
|!          |1      |boolean        |logisches Komplement
|(Typ)      |1      |jeder          |Cast
|*, /, %    |2      |arithmetisch   |Multiplikation, Division, Rest
|+, -       |3      |arithmetisch   |binärer Operator für Addition und Subtraktion
|+          |3      |String         |String-Konkatenation
|<<         |4      |integral       |Verschiebung nach links
|>>         |4      |integral       |Rechtsverschiebung mit Vorzeichenerweiterung
|>>>        |4      |integral       |Rechtsverschiebung ohne Vorzeichenerweiterung
|<, \<=, >, >=  |5   |arithmetisch  |numerische Vergleiche
|instanceof |5      |Objekt         |Typvergleich
|==, !=     |6      |primitiv       |Gleich-/Ungleichheit von Werten
|==, !=     |6      |Objekt         |Gleich-/Ungleichheit von Referenzen
|&          |7      |integral       |bitweises Und
|&          |7      |boolean        |logisches Und
|^          |8      |integral       |bitweises XOR
|^          |8      |boolean        |logisches XOR
| \|        |9      |integral       |bitweises Oder
| \|        |9      |boolean        |logisches Oder
|&&         |10     |boolean        |logisches konditionales Und, Kurzschluss
|\|\|       |11     |boolean        |logisches konditionales Oder, Kurzschluss
|?:         |12     |jeder          |Bedingungsoperator
|=          |13     |jeder          |Zuweisung
|*=, /=, %=, +=, =, <<=, >>=, >>>=, &=, ^=, \|=
            |14     |arithmetisch   |Zuweisung mit Operation
|+=         |14     |String         |Zuweisung mit String-Konkatenation
|===

[appendix]

== Turtle

[%autowidth]
|===
|Befehl  |Beschreibung |Beispiele

|`forward`
|Bewege dich um so viele Pixel vorwärts
|`forward(100)` +
`forward(2.5)`

|`back`
|Bewege dich um so viele Pixel rückwärts
|`back(50)` +
`back(200)`

|`left`
|Drehe dich um so viele ° nach links
|`left(90)` +
`left(22.5)`

|`right`
|Drehe dich um so viele ° nach rechts
|`right(45)` +
`right(120)`

|`penUp`
|Bewege dich ab jetzt ohne zu zeichnen
|`penUp()`

|`penDown`
|Zeichne ab jetzt wieder beim Bewegen
|`penDown()`

|`setPenColor`
|Verwende ab jetzt eine andere Stiftfarbe. Farben werden als Strings angegeben, entweder als https://de.wikipedia.org/wiki/Webfarbe#CSS_3[englischer Name], als Hex-String oder mittels RGB- oder HSL-Format.
|`setPenColor("blue")` +
`setPenColor("HotPink")` +
`setPenColor("#ff6688")` +
`setPenColor("rgb(255,102,136)")` +
`setPenColor("hsl(240,100%,100%)")` +

|`setPenWidth`
|Verwende ab jetzt eine andere Stiftbreite. Die Breite wird in Pixel angegeben; der Standardwert ist 1.
|`setPenWidth(3)` +
`setPenWidth(0.5)` +

|`setSpeed`
|Bewege dich ab jetzt schneller oder langsamer. Die Geschwindigkeit wird in Pixel pro Sekunde angegeben; der Standardwert ist 100.
|`setSpeed(50)` +
`setSpeed(1000)`
|===

== Keywords (verbotene Bezeichner)

.Wörter die in Java nicht als Bezeichner (z.B. Variablennamen und Methodennamen) verwendet werden dürfen.
[font="mono"]
[cols="m,m,m,m,m,m"]
|===
|_          |class      |false      |instanceof |protected      |this
|abstract   |const      |final      |int        |public         |throw
|assert     |continue   |finally    |interface  |return         |throws
|boolean    |default    |float      |long       |short          |transient
|break      |do         |for        |native     |static         |true
|byte       |double     |goto       |new        |strictfp       |try
|case       |else       |if         |null       |super          |void
|catch      |enum       |implements |package    |switch         |volatile
|char       |extends    |import     |private    |synchronized   |while
|===
