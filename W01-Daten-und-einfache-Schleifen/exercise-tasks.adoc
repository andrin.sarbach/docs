= Übungsaufgaben
:sectnums:
:stem: latexmath
:icons: font

NOTE: Here are some small exercises for you to solve on your own next week. Use the class time in the EIPR module for this and take the opportunity to ask your EIPR lecturers questions right away. +
You have already solved a few of the tasks during the OOP1 lessons. These are listed here again for completeness and marked accordingly.


== Strings and comments (lecture)

Decide for the following pieces of code whether they are valid and, if so, what output they produce:

[loweralpha]
. `+System.out.println(/*"Hi"*/);+`
. `+System.out.println("println()");+`
. `+// System.out.println("Hi");+`
. `+/**/ System.out.println("//Hi");+`
. `+/* System.out.println("Hi */");+`
. `+System.out.println("println("Hi")");+`

== Draw with the turtle (lecture)

Write a turtle program, which produces the following drawing:

[loweralpha]
. image:res/turtle-e.png[width=150]
. image:res/turtle-star.png[width=200]

*Hint:* Use the following `import` command at the very beginning of the program (before `public class`):

[source,java]
----
import static ch.trick17.turtle4j.TurtleGraphics.*;
----

Also note that the Turtle library is only available in the provided template project; if you create your own project in IntelliJ, you cannot (readily) use the turtle commands.


== Reading expressions

Evaluate the following expressions by hand and then check them by writing a small program that outputs them with `System.out.println(...)`.

[loweralpha]
. `23 / 7`
. `2*3 + 6/4`
. `2+3 * 4-6`
. `(12 + 3) / 4 * 2`
. `720310 / 10 / 10 / 10 / 10`
. `- 10 - 20 - - 40`
. `5.0 / 2 * 3 / 2`
. `10.0 / 2 / 4`
. `1.5 + 3 * 1.5`
. `53 / 5 / (0.6 + 1.4) / 2 + 13 / 2`


== The Modulo operator

The modulo operator `%` calculates the remainder of an integer division, e.g. 19 ÷ 5 = 3, *remainder 4.* Evaluate the following expressions by hand and check again with `println(...)` outputs.

[loweralpha]
. `17 % 6`
. `300 % 60`
. `917 % 100`
. `(238 % 10 + 3) % 2`
. `26 % 10 % 4 * 3`
. `177 % 100 % 10 / 2`
. `1 + 10 % 10 + 1`


== Which data type fits? (lecture)

Which data type would you use to store the following information?

[loweralpha]
. a temperature measurement in °C
. the number of children in a family
. the average number of children per couple
. the answer to: "Are you pregnant?"
. the amount of money in your bank account
. the number of trees on earth


== Selecting variable names

What variable names would you use for the information in the previous exercise?


== Strings vs. Variables (lecture)

What is the difference between the following four code snippets? Are all of them valid? What output do they produce?

[loweralpha]
. {empty}
+
[source,java]
----
int hello;
hello = 42;
System.out.println(hello);
----

. {empty}
+
[source,java]
----
int hello;
hello = 42;
System.out.println("hello");
----

. {empty}
+
[source,java]
----
int "hello";
"hello" = 42;
System.out.println("hello");
----

. {empty}
+
[source,java]
----
int "hello";
"hello" = 42;
System.out.println(hello);
----


== Valid variable names

What are valid variable names? Try to give the answers first without any tools and check them finally by writing a small program containing variables with these names and observe if the compiler reports errors.

[loweralpha]
. `yes`
. `firstName`
. `last-name`
. `println`
. `"hello"`
. `for`
. `42isTheAnswer`
. `sum_of_data`
. `_foo`
. `b4`
. `I_AM_GROOT`
. `$money$`
. `System.out`
. `unprotected`


== Reading programs with variables (lecture)

What is the value of the variables `number` and `x` after each of the following statements?

[source,java]
----
int number = 1;
int x = 3;
number = x * 5;
x = 20;
number = number + 3;
number = x + number * 2;
----

== Reading programs with variables

What is the value of the variables `i`, `j` and `k` after the following piece of code?

[source,java]
----
int i = 2, j = 3, k = 4;
int x = i + j + k;
i = x - i - j;
j = x - j - k;
k = x - i - k;
----

== From the formula to the expression

You have a variable with the real number stem:[x]. Write an expression that calculates stem:[y]:

[stem]
++++
y=9.1x^3+19.3x^2-4.6x+34.2
++++

*Additional exercise:* Try to find an expression that uses _only three_ `*`&#8209;operators (and some `+`).

== Duplicate calculations

Modify the following program so that it does not perform the same calculation more than once. 
Use variables with appropriate types and names to cache the results of these calculations.

[source,java]
----
System.out.println("Working hours:");
System.out.println(4 + 5 + 8 + 4);

System.out.println("Hourly salary:");
System.out.println("25.50 CHF");

System.out.println("Total salary:");
System.out.println((4 + 5 + 8 + 4) * 25.50);

System.out.println("Taxes:"); // 20% taxes...
System.out.println((4 + 5 + 8 + 4) * 25.50 * 0.20);
----


== From square to circle (lecture)

[loweralpha]
. Write a turtle program with a `for` loop that draws a square with a perimeter of 600.
. Modify the program so that it draws a hexagon with the same perimeter.
. Change the program again so that it draws a a shape with 13 angles, again with perimeter 600. Try to write the program so that, to change the number of corners in the future, you only have to change one place in the code.
. Change the program one last time so that it draws a shape with 360 angles. What do you find?
