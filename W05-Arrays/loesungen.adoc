= Lösungen der Übungsaufgaben
:sectnums:
:stem: latexmath

== Programme mit Arrays lesen (Vorlesung)

[loweralpha]
. {empty}
+
----
2
6
10
----
. {empty}
+
----
20
----
. {empty}
+
----
24
----
. {empty}
+
----
0
----


== Weitere Programme lesen

[loweralpha]
. `[0, 4, 11, 0, 44, 0, 0, 2]`
. `[3, 24, 8, -5, 6, 1]`


== Mit Arrays Programmieren (Vorlesung)

Lösung in Ihrem persönlichen Git-Repository, im Ordner «aufgaben-05-loesung».


== Wert- und Referenztypen

----
2 [0, 0, 1, 0]
1 [0, 0, 1, 0]
3 [0, 0, 1, 1]
2 [0, 0, 1, 1]
----


== `null`-Probleme (Vorlesung)

[loweralpha]
. `NullPointerException`, bei `text.charAt(0)`. Das Array `texte` wird _Null-initialisiert_ und enthält deshalb lauter `null`-Einträge. Ein Methodenaufruf auf einer `null`-Referenz führt zur Exception.
. Ausgabe `0`. In Variable `s` zwar zuerst `null` gespeichert, aber danach wird dieser Wert mit `namen[3]` überschrieben, was einen leeren String enthält. Leere Strings erzeugen keine `NullPointerException`, sondern geben als Länge einfach 0 zurück.
. Leere Ausgabe, aber keine Exception. Array ist leer, deshalb für die `for`-Schleife gar keine Iteration durch.
. `NullPointerException`, bei `data[1].length`, da in `data[1]` zuvor `null` gespeichert wurde.


== Komplexe Array-Schleifen

[loweralpha]
. `[20, 30, 40, 50, 50]`
. `[10, 10, 10, 10, 10]`


== __n__D-Array ausgeben (Vorlesung)

[source,java]
----
public static void print(int[][] grid) {
    for (int i = 0; i < grid.length; i++) {
        for (int j = 0; j < grid[i].length; j++) {
            System.out.print(grid[i][j] + " ");
        }
        System.out.println();
    }
}
----


== Array-Vergleiche

[source,java]
----
public static boolean alleKleiner(int[] list1, int[] list2) {
    if (list1.length != list2.length) {
        return false;
    }
    for (int i = 0; i < list1.length; i++) {
        if (list1[i] >= list2[i]) {
            return false;
        }
    }
    return true;
}
----
