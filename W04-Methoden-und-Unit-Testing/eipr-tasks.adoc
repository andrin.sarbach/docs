= EIPR-Aufgaben
:sectnums:
:stem: latexmath
:icons: font

== Testing with JUnit

Until now, you have tested your programs "by hand", i.e. you have run the program several times and tried out different inputs. Starting this week, you are to (also) test your programs automatically, with _JUnit_.

The program `PerpetualCalendar` calculates the day of the week for each (valid) date, but unfortunately it still contains some logic errors. Fortunately, the author has written tests that check the correctness of the parts of the program. You can find them in the class `PerpetualCalendarTest` in the folder "test". Use these tests to find and fix the errors in the program.

[loweralpha]
. Run the program `PerpetualCalendar`. Enter some data and check the output. You will find that the program does not work correctly yet.

. Open the class `PerpetualCalendarTest` and press the play icon next to the class name to run all the tests in this class. The _Run_ tab opens at the bottom and the tests are executed. In the right-hand area you will see "Tests failed: 4, passed 1". So 5 tests have been run, 4 of which failed.
+
[.text-center]
image::res/tests-failed.png[width=400]

. In the left-hand area of the _Run_ tab you will see a listing of the tests. There is a test for each auxiliary method in the program. At the moment only the `isLeapYear()` method seems to be correct. Click on a failed test to see the error in the right-hand pane of the view. The test `testCountDaysInYear` says
+
----
org.opentest4j.AssertionFailedError:
Expected :365
Actual   :366
----

. Double-click on `testCountDaysInYear` on the left to jump to the code for this test. The method `countDaysInYear()` of the class `PerpetualCalendar` is called with the argument `1900`. Furthermore, `assertEquals()` is called, which compares the result of `countDaysInYear()` with the expected value `365`. So the fact that the test fails on this line means that `countDaysInYear(1900)` does not return the expected value `365`.

. Go back to the `PerpetualCalendar.countDaysInYear()` method. Find and fix the error in this method and run the tests again. If you have fixed the error correctly, the `testCountDaysInYear` test will run without error and it will say "Tests failed: 3" at the top.

. Now find the remaining errors by following the failing tests. At the end, all tests should be displayed in green:
+
[.text-center]
image::res/tests-passed.png[width=400]
