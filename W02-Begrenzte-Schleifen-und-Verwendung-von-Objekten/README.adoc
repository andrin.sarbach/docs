= Woche 2 -- Begrenzte Schleifen und Verwendung von Objekten

link:Begrenzte%20Schleifen%20und%20Verwendung%20von%20Objekten.pdf[Vorlesungsfolien]

xref:skript.adoc[Skript]


== Aufgaben

xref:uebungsaufgaben.adoc[Übungsaufgaben] und xref:loesungen.adoc[Lösungen] (xref:exercise-tasks.adoc[Exercise Tasks] and xref:solutions.adoc[Solutions])

xref:programmieraufgaben.adoc[Programmieraufgaben] (xref:programming-tasks.adoc[Programming Tasks])

xref:eipr-aufgaben.adoc[EIPR-Aufgaben] (xref:eipr-tasks.adoc[EIPR-Tasks])

xref:repetitionsaufgaben.adoc[Repetitionsaufgaben]


== Obligatorische Aufgabe

Die obligatorische Aufgabe für diese Woche ist xref:programmieraufgaben.adoc#_kunst_mit_ascii_zeichen[«Kunst» mit ASCII-Zeichen] (xref:programming-tasks.adoc#_making_art_with_ascii_characters[Making «art» with ASCII characters]). Der Abgabetermin ist am *01.10.2023, um 23:59*.


== Standortbestimmung

Der Abgabetermin für das Quiz von dieser Woche ist ebenfalls am *01.10.2023, um 23:59*.

→ https://api.socrative.com/rc/atNwWg[Zum Quiz für Woche 2]

Sie verwenden denselben Zugangscode («Student ID») wie letzte Woche.

Falls Sie letzte Woche eine niedrige Punkzahl im Quiz erreicht haben (weniger als 60 %), empfehlen wir Ihnen, das https://api.socrative.com/rc/UBisFS[Quiz von Woche 1] gegen Ende der Woche nochmals durchzuführen.
